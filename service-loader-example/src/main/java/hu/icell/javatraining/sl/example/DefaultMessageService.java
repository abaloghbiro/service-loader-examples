package hu.icell.javatraining.sl.example;

public class DefaultMessageService implements MessageService {

	public void printMessage(String message) {
		System.out.println("Default service : " + message);
	}
}

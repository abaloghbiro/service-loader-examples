package hu.icell.javatraining.sl.example;

import java.util.ServiceLoader;

public class Main {

	public static void main(String[] args) {

		ServiceLoader<MessageService> services = java.util.ServiceLoader.load(MessageService.class);
		services.forEach(service -> {

			service.printMessage("Hello World");

		});

	}
}

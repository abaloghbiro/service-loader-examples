package hu.icell.javatraining.sl.example;

public class SecondMessageService implements MessageService {

	public void printMessage(String message) {
		System.out.println("Second service : " + message);
	}
}

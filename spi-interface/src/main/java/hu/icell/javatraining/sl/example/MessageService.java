package hu.icell.javatraining.sl.example;

public interface MessageService {

	void printMessage(String message);
}

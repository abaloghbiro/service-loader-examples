package hu.icell.javatraining.sl.example;

public class ThirdMessageService implements MessageService {

	public void printMessage(String message) {
		System.out.println("Third service : " + message);
	}
}
